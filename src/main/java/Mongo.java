import com.mongodb.client.MongoDatabase;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Mongo {

    public static void main(String[] args) {

        String url = "mongodb+srv://droroguna:Infor2022@cluster0.u9tkfiu.mongodb.net/?retryWrites=true&w=majority";
        String database = "Pokemon";

        Conexion conexion = new Conexion();
        MongoDatabase db = conexion.connect(url, database);

        Metodos m = new Metodos();

        Scanner scanner = new Scanner(System.in);
        int opcion;

        do {
            System.out.println("------ Menú ------");
            System.out.println("1. Buscar datos de pokemon");
            System.out.println("2. Eliminar pokemon");
            System.out.println("3. Insertar nuevo pokemon");
            System.out.println("4. Actualizar pokemon");
            System.out.println("5. Contar pokemons");
            System.out.println("6. Salir");
            System.out.print("Selecciona una opción: ");

            try {
                opcion = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Por favor, ingresa un número válido.");
                scanner.nextLine();
                opcion = 0;
            }

            switch (opcion) {
                case 1:
                    m.buscar(db, "name");
                    break;
                case 2:
                    System.out.println("Pulsa 1 para eliminar 1, pulsa 2 para eliminar varios");
                    int opcion2;
                    try {
                        opcion2 = scanner.nextInt();
                    } catch (InputMismatchException e) {
                        System.out.println("Por favor, ingresa un número válido.");
                        scanner.nextLine(); // Limpiar el buffer del scanner
                        opcion2 = 0; // Establecer opción en 0 para evitar un bucle infinito
                    }
                    switch (opcion2) {
                        case 1:
                            m.eliminar(db, "name");
                            break;
                        case 2:
                            m.eliminarVarios(db, "name");
                            break;
                        default:
                            System.out.println("Opción no válida. Inténtalo de nuevo.");
                            break;
                    }
                    break;
                case 3:
                    m.insertar(db, "name");
                    break;
                case 4:
                    m.actualizar(db, "name");
                    break;
                case 5:
                    m.contar(db);
                    break;
                case 6:
                    System.out.println("Saliendo del programa. ¡Hasta luego!");
                    break;
                default:
                    System.out.println("Opción no válida. Inténtalo de nuevo.");
                    break;
            }
        } while (opcion != 6);

        scanner.close();
    }
}
