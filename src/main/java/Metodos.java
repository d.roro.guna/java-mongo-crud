import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;

import java.util.Scanner;


public class Metodos {
    Scanner sc = new Scanner(System.in);

    public void contar(MongoDatabase db){
        int contador = 0;
        MongoCollection<Document> collection = db.getCollection("Pokemon");

        MongoCursor<Document> cursor = collection.find().iterator();

        while (cursor.hasNext()) {
            Document document = cursor.next();
             contador++;
            if (document.containsKey("name")) {
                System.out.println("Nombre : "+ document.get("name"));
            }
        }
        System.out.println("Hay "+contador+" pokemons");
    }


    public void buscar(MongoDatabase db, String key){

        System.out.println("Introduce el nombre a buscar");String resp = sc.next();

        MongoCollection<Document> collection1 = db.getCollection("Pokemon");

        Document findDoc = new Document(key,resp);

        MongoCursor<Document> resultDocument = collection1.find(findDoc).iterator();

        System.out.println("Listado de todos los resultados: ");
        while(resultDocument.hasNext()){
            System.out.println("nombre "+resultDocument.next());
        }

    }

    public void eliminar(MongoDatabase db,String key){
        System.out.println("Introduce el nombre a eliminar");String resp = sc.next();

        MongoCollection<Document> collection1 = db.getCollection("Pokemon");
        Document doc = new Document(key,resp);

        collection1.deleteOne(doc);

    }

    public void eliminarVarios(MongoDatabase db,String key){
        System.out.println("Introduce el nombre a eliminar");String resp = sc.next();

        MongoCollection<Document> collection1 = db.getCollection("Pokemon");

        collection1.deleteMany(Filters.eq(key, resp));

    }

    public void insertar(MongoDatabase db,String key){
        System.out.println("Introduce el nombre a insertar");String resp = sc.next();

        MongoCollection<Document> collection1 = db.getCollection("Pokemon");

        Document pokemonDocument = new Document(key, resp);

        collection1.insertOne(pokemonDocument);

    }


    public void actualizar(MongoDatabase db,String key){
        System.out.println("Introduce el pokemon a actualizar"); String resp = sc.next();

        MongoCollection<Document> collection1 = db.getCollection("Pokemon");

        System.out.println("Introduce el nuevo nombre"); String nuevoNombre = sc.next();

        Document filtro = new Document(key, resp);

        Document actualizacion = new Document("$set", new Document(key, nuevoNombre));

        collection1.updateOne(filtro, actualizacion);

    }


}
